# fillenv

## About

`fillenv` is a environment variables manager, designed to be invoked in the shell. `fillenv` works on any POSIX-compliant shell (sh, dash, ksh, zsh, bash), in particular on these platforms: unix, macOS, and windows WSL.
## Prerequisites

`fillenv` requires a O/S which:

- includes the `bash` shell

Environments that should work include:

- Linux (RHEL/CentOS, Debian/Ubuntu, Amazon Linux)
- macOS 10.11 (El Capitan) and later
- Windows 10 with Ubuntu on Windows
- Docker using a supported Linux distro

## Instalation

Feched via `curl`:

```bash
curl -o- https://unpkg.com/fillenv@1.0.0-node-beta.4/fillenv.sh | bash
```

Installed via `npx`:

```bash
npx fillenv@node-beta
```