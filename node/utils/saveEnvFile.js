const fs = require('fs/promises')
const inquirer = require('inquirer')
const reduceVariablesToFile = require('./reduceVariablesToFile')
const { fileColor } = require('./colorize')

const cwd = process.cwd()

async function saveEnvFile(envType, variables) {
	const filePath = `${cwd}\\${envType}`

	console.log(`About to create ${fileColor(filePath)}:\n`)

	const finalText = reduceVariablesToFile(variables)

	console.log(finalText)

	console.log('')

	const { done } = await inquirer.prompt({
		type: 'confirm',
		name: 'done',
		message: 'Is this ok?',
	})

	if (done) {
		await fs.writeFile(filePath, finalText)
		console.log("File created!")
	} else {
		console.log("Ok, aborted.")
	}
}

module.exports = saveEnvFile
