function convertComment(comment) {
	return comment.trim().split("\n").map(x => ('# '+x).trim()).join("\n")
}

function reduceVariablesToFile(variables) {
	return variables
		.reduce((prev, cur) => {
			if (typeof cur === 'object') {
				return prev +
					`${typeof cur.comment === 'string' && cur.comment.trim() !== '' ? `${convertComment(cur.comment)}\n` : ''}${cur.key.trim()}=${cur.value.trim()}\n`
			} else if (typeof cur === 'number') {
				return prev + (new Array(cur).fill("\n")).join("")
			} else if (typeof cur === 'string') {
				return prev + convertComment(cur) + '\n'
			}
		}, '').trim()
}

module.exports = reduceVariablesToFile
