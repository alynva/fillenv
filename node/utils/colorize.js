// Console colors: 
// - https://stackoverflow.com/a/41407246
// - https://en.m.wikipedia.org/wiki/ANSI_escape_code

const COLORS = {
	Black: 30,
	Red: 31,
	Green: 32,
	Yellow: 33,
	Blue: 34,
	Magenta: 35,
	Cyan: 36,
	White: 37,
	"Bright Black": 90,
	"Bright Red": 91,
	"Bright Green": 92,
	"Bright Yellow": 93,
	"Bright Blue": 94,
	"Bright Magenta": 95,
	"Bright Cyan": 96,
	"Bright White": 97,
}


function colorize(color, text) {
	return `\x1b[${color}m${text}\x1b[0m`
}

function fileColor(name) {
	return colorize(COLORS.Magenta, name)
}

module.exports = colorize
module.exports.COLORS = COLORS
module.exports.fileColor = fileColor
