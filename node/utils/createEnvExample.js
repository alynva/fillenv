const inquirer = require('inquirer')
const saveEnvFile = require('./saveEnvFile')

async function createEnvExample() {
	const newEnv = []
	let more
	do {
		more = false
		const { key } = await inquirer.prompt({
			type: 'input',
			name: 'key',
			message: `${newEnv.length + 1}ª key:`,
			validate: x => x.trim() !== '' || 'You must provide the key.',
		})

		const { value } = await inquirer.prompt({
			type: 'input',
			name: 'value',
			message: `${key.trim()}' value:`,
		})

		const { comment } = await inquirer.prompt({
			type: 'input',
			name: 'comment',
			message: `${key.trim()}' comment (leave empty to skip):`,
		})

		newEnv.push({
			key: key.trim(),
			value: value.trim(),
			comment: comment.trim(),
		})

		more = (await inquirer.prompt({
			type: 'confirm',
			name: 'more',
			message: 'Do you want add more variables?',
			default: false
		})).more
	} while (more)

	await saveEnvFile('.env.example', newEnv)
}

module.exports = createEnvExample
