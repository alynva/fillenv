function parseFileToVariables(fileText) {
	const features = []
	let reading = reference = 0
	for (const line of fileText.split("\n")) {
		if (line.startsWith("#")) {
			const comment = line.slice(1).trim()
			if (typeof features[reference] === 'string') {
				features[reference] = [features[reference], comment].join("\n")
			} else if (typeof features[reference] === 'undefined') {
				features[reference] = comment
			} else if (["number", "object"].includes(typeof features[reference])) {
				reference++
				features[reference] = comment
			}
		} else if (line.trim() === "") {
			if (["string", "object"].includes(typeof features[reference])) {
				reference++
				features[reference] = 1
			} else if (typeof features[reference] === 'number') {
				features[reference] = features[reference] + 1
			} else {
				throw new Error("Unexpected behaviour")
			}
		} else {
			// TODO: use regex
			const variable = line.split("="),
				finalVariable = {
					key: variable[0].trim(),
					value: variable[1].trim(),
				}

			if (["string", "undefined"].includes(typeof features[reference])) {
				if (typeof features[reference] === 'string') {
					finalVariable.comment = features[reference]
				}

				features[reference] = finalVariable
			} else if (typeof features[reference] === 'number') {
				reference++
				features[reference] = finalVariable
			}
		}
		reading++
	}

	return features
}

module.exports = parseFileToVariables
