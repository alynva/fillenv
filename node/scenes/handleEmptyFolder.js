const inquirer = require('inquirer');
const createEnvExample = require('../utils/createEnvExample')
const { fileColor } = require('../utils/colorize')

async function handleEmptyFolder() {
	console.log('-- Start of the scene --')

	console.log(`No ${fileColor('.env*')} files found.`)
	
	const { confirm } = await inquirer.prompt({
		type: 'confirm',
		name: 'confirm',
		message: `Do you want the initialize an ${fileColor('.env.example')} now?`,
	})

	if (confirm) {
		await createEnvExample()
	} else {
		console.log('Ok.')
	}

	console.log('-- End of the scene --')
}

module.exports = handleEmptyFolder
