const fs = require('fs/promises')
const inquirer = require('inquirer')
const { fileColor } = require('../utils/colorize')
const parseFileToVariables = require('../utils/parseFileToVariables')
const saveEnvFile = require('../utils/saveEnvFile')

async function handleExampleAndEnvButOutOfSync() {
	console.log('-- Start of the scene --')
	
	console.log(`Was found an ${fileColor('.env*')} and an ${fileColor('.env.example')} but the variables are not the same in both files.`)

	const curExampleFile = await (await fs.readFile('./.env.example')).toString()
	const curEnvFile = await (await fs.readFile('./.env')).toString()
	// TODO: read it from file
	const curExample = parseFileToVariables(curExampleFile)
	const curEnv = parseFileToVariables(curEnvFile)
	// const curEnv = [{
	// 	key: 'APP_NAME',
	// 	value: 'Otavs',
	// }, {
	// 	key: 'API_KEY',
	// 	value: 'None',
	// 	comment: 'Access key of Google Maps',
	// }, {
	// 	key: 'THEME',
	// 	value: 'night',
	// }]
	// const curExample = [{
	// 	key: 'APP_NAME',
	// 	value: '',
	// }, {
	// 	key: 'API_KEY',
	// 	value: '',
	// 	comment: 'Access key of Google Maps',
	// }, {
	// 	key: 'CLASS',
	// 	value: '',
	// }]

	const allVariables = {}

	curEnv.forEach(v => {
		if (typeof v === 'object') allVariables[v.key] = ['env']
	})

	curExample.forEach(v => {
		if (typeof v === 'object') allVariables[v.key] = [...(allVariables[v.key] || []), 'example']
	})

	const { update } = await inquirer.prompt({
		type: 'confirm',
		name: 'update',
		message: `Did you want to update they?`,
	})

	if (update) {
		let choices = []
		Object.keys(allVariables).forEach(k => {
			if (allVariables[k].length === 1) {
				choices.push(new inquirer.Separator(`== Variable ${k} ==`))
				choices.push({
					name: `Remove ${k} from ${allVariables[k]}`,
					value: `r|${k}`
				})
				const missing = allVariables[k].includes('env') ? 'example' : 'env'
				choices.push({
					name: `Add ${k} in ${missing}`,
					value: `a|${k}`
				})
			}
		})

		const { tasks } = await inquirer.prompt({
			type: 'checkbox',
			name: 'tasks',
			choices,
			message: 'What you want to do?',
			validate: input => {
				if (input.length !== choices.length / 3) return "Choose one task for each variable."

				const duplicates = input
					.map(a => a.slice(2))
					.filter((v, i, s) => s.indexOf(v) !== i)

				if (duplicates.length) return "Choose only one task per variable."

				return true
			}
		})

		const vars2Modify = tasks
			.reduce((prev, cur) => ({
				...prev,
				[cur.split("|")[1]]: cur.split("|")[0],
			}), {})
		
		let newEnv = [...curEnv]
		const newExample = [...curExample]

		let askToFill = false
		
		Object.keys(vars2Modify).forEach(k => {
			let index
			if ((index = curEnv.findIndex(v => v.key === k)) > -1) {
				switch (vars2Modify[k]) {
					case 'r':
						newEnv.splice(index, 1)
						break;
				
					case 'a':
					default:
						newExample.push({
							...curEnv[index],
							value: '',
						})
						break;
				}
			}

			if ((index = curExample.findIndex(v => v.key === k)) > -1) {
				switch (vars2Modify[k]) {
					case 'r':
						newExample.splice(index, 1)
						break;
				
					case 'a':
					default:
						newEnv.push({
							...curExample[index],
						})

						if (curExample[index].value === '')
							askToFill = true
						break;
				}
			}
		})

		// TODO: ask if want to sync "floating" comments
		newEnv = newExample.map(v => typeof v === 'object' ? newEnv.find(v2 => v2.key === v.key) : v)

		if (askToFill) {
			console.log("Some variables from the example come empty.")

			const { refill } = await inquirer.prompt({
				type: 'confirm',
				name: 'refill',
				message: `Do you want to refill the ${fileColor(".env")}?`,
			})

			if (refill) {
				const refilled = []

				for (const variable of newEnv) {
					if (typeof variable === 'object') {
						if (variable.value !== '') {
							refilled.push({ ...variable })
							continue
						}

						const message = variable.comment ? variable.comment + ` [${variable.key}]:` : variable.key + ':'

						const { value } = await inquirer.prompt({
							type: 'input',
							name: 'value',
							message,
						})

						refilled.push({ ...variable, value })
					} else {
						refilled.push(variable)
					}
				}

				newEnv = refilled
			}
		}
		
		await saveEnvFile('.env', newEnv)
		await saveEnvFile('.env.example', newExample)
	}

	console.log('-- End of the scene --')
}

module.exports = handleExampleAndEnvButOutOfSync