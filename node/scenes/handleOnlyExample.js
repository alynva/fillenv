const fs = require('fs/promises')
const inquirer = require('inquirer')
const saveEnvFile = require('../utils/saveEnvFile')
const { fileColor } = require('../utils/colorize')
const parseFileToVariables = require('../utils/parseFileToVariables')

async function handleOnlyExample() {
	console.log('-- Start of the scene --')

	console.log(`Was found only an ${fileColor('.env.example')} here:\n`)

	const curExampleFile = await (await fs.readFile('./.env.example')).toString()

	console.log(curExampleFile.trim())

	console.log('')

	const { initialize } = await inquirer.prompt({
		type: 'confirm',
		name: 'initialize',
		message: `Did you want to initialize a new ${fileColor('.env')} file with this example?`,
	})

	if (initialize) {
		const curExample = parseFileToVariables(curExampleFile)
		const newEnv = []

		for (const variable of curExample) {
			if (typeof variable === 'object') {
				const message = variable.comment ? variable.comment + ` [${variable.key}]:` : variable.key + ':'

				const { value } = await inquirer.prompt({
					type: 'input',
					name: 'value',
					message,
					default: variable.value,
				})

				newEnv.push({ ...variable, value })
			} else {
				newEnv.push(variable)
			}
		}

		await saveEnvFile('.env', newEnv)
	}

	console.log('-- End of the scene --')
}

module.exports = handleOnlyExample
