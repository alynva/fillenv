const fs = require('fs/promises')
const inquirer = require('inquirer')
const saveEnvFile = require('../utils/saveEnvFile')
const { fileColor } = require('../utils/colorize')
const parseFileToVariables = require('../utils/parseFileToVariables')

async function handleOnlyEnv() {
	console.log('-- Start of the scene --')

	console.log(`Was found an ${fileColor('.env*')} here but none ${fileColor('.env.example')}:\n`)
	
	const curEnvFile = await (await fs.readFile('./.env')).toString()
	console.log(curEnvFile.trim())

	console.log('')

	const { initialize } = await inquirer.prompt({
		type: 'confirm',
		name: 'initialize',
		message: `Did you want to generate a new ${fileColor('.env.example')} file from this?`,
	})

	if (initialize) {
		const curEnv = parseFileToVariables(curEnvFile)
		const choices = curEnv.filter(x => typeof x === 'object').map(x => ({
			value: x.key,
			name: x.comment ? x.comment + ` [${x.key}]` : x.key,
		}))

		const { erase } = await inquirer.prompt({
			type: 'checkbox',
			name: 'erase',
			choices,
			message: 'Which variables you want to leave empty in the example?'
		})

		const newExample = curEnv.map(variable => typeof variable === 'object' ? ({
			...variable,
			value: erase.includes(variable.key) ? '' : variable.key,
		}) : variable)

		await saveEnvFile('.env.example', newExample)
	}

	console.log('-- End of the scene --')
}

module.exports = handleOnlyEnv