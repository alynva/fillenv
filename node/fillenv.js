#!/usr/bin/env node

const inquirer = require('inquirer');
const handleEmptyFolder = require('./scenes/handleEmptyFolder')
const handleOnlyExample = require('./scenes/handleOnlyExample')
const handleOnlyEnv = require('./scenes/handleOnlyEnv')
const handleExampleAndEnvButOutOfSync = require('./scenes/handleExampleAndEnvButOutOfSync')
const colorize = require('./utils/colorize')

// Font generator: http://patorjk.com/software/taag
// Based on the font ANSI Shadow
console.log(colorize(colorize.COLORS.Cyan, `
    ███████ ██ ██      ██          ╔══════╗╔══╗  ╔═╗╔═╗   ╔═╗
    ██      ██ ██      ██          ║ ╔════╝║  ╚╗ ║ ║║ ║   ║ ║
    █████   ██ ██      ██          ║ ╚══╗  ║ ╔╗╚╗║ ║║ ║   ║ ║
    ██      ██ ██      ██          ║ ╔══╝  ║ ║╚╗╚╝ ║╚╗╚╗ ╔╝╔╝
    ██      ██ ███████ ███████     ║ ╚════╗║ ║ ╚╗  ║ ╚╗╚═╝╔╝ 
                                   ╚══════╝╚═╝  ╚══╝  ╚═══╝  
`))

console.log('Hi, let\'s fill these variables!')
console.log()

var scene = {
	type: 'list',
	name: 'scene',
	message: 'Wich scene you want to test?',
	choices: [{
		value: 0,
		name: 'Without .env* files',
	}, {
		value: 1,
		name: 'Only one .env.example',
	}, {
		value: 2,
		name: 'Only one .env',
	}, {
		value: 3,
		name: 'One .env.example and one .env but out of sync',
	}],
}

inquirer.prompt(scene).then(answer => [
	handleEmptyFolder, handleOnlyExample, handleOnlyEnv,
	handleExampleAndEnvButOutOfSync,
][answer.scene]())
